# Documentator
###### Document reporter for Google Sheets

This app do the simple thing: takes data of [`Google Forms`](https://www.google.com/intl/en/forms/about/), 
insert it to the specific template and save it in `.docx` file.
