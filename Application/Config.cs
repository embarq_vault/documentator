﻿using System;

namespace App
{
    class Config
    {
        public static System.Windows.Forms.ListView 
            ConfigurateListView(System.Windows.Forms.ListView item)
        {
            item.Activation = System.Windows.Forms.ItemActivation.OneClick;
            item.AllowColumnReorder = true;
            item.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            item.CheckBoxes = true;
            item.FullRowSelect = true;
            item.GridLines = true;
            item.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            item.HoverSelection = true;
            item.ShowItemToolTips = true;
            item.Size = new System.Drawing.Size(552, 247);
            item.UseCompatibleStateImageBehavior = false;
            item.View = System.Windows.Forms.View.Details;
            return item;
        }
    }
}
