﻿using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SheetAPIAdapter;

namespace App
{
    public partial class Main : Form
    {
        private Spreadsheet spreadsheet;
        private string CurrentSheet;

        public Main()
        {
            InitializeComponent();
            InitializeContentAsync();
        }

        private async void InitializeContentAsync()
        {
            await InitializeContentAsync_Runner();
        }

        private Task InitializeContentAsync_Runner()
        {
            return Task.Run(() => {
                InitializeContent();
            });
        }

        private void InitializeContent()
        {
            try
            {
                spreadsheet = new Spreadsheet("test-sheet");
            }
            catch (System.IO.FileNotFoundException err)
            {
                MessageBox.Show(err.Message);
                Application.Exit();
            }
            finally
            {
                CurrentSheet = spreadsheet.Titles[0];
                currentSheetLabel.Text = CurrentSheet;
                foreach (var sheet in spreadsheet.Sheets)
                {
                    selectSheetToolStripMenuItem.DropDownItems.Add(sheet.Key);
                }
                PrintSheet();

            }
        }

        delegate void PrintSheetCallback();

        private void PrintSheet()
        {
            Sheet sheet = spreadsheet.Sheets[CurrentSheet];

            sheetView.Clear();

            if (sheetView.InvokeRequired)
            {
                PrintSheetCallback callback = new PrintSheetCallback(PrintSheet);
                Invoke(callback, new object[] { });
            }
            else
            {
                foreach (var headerContent in sheet.Captions)
                {
                    if (!headerContent.Equals("exported"))
                    {
                        var header = new ColumnHeader();

                        header.Text = headerContent;
                        header.TextAlign = HorizontalAlignment.Center;

                        sheetView.Columns.Add(header);
                    }
                }

                foreach (var item in sheet.Data)
                {
                    var items = new List<string>();

                    foreach (var subitem in item.Value)
                    {
                        if (!subitem.Key.Equals("exported"))
                        {
                            items.Add(subitem.Value);
                        }
                    }

                    var newItemForeColor = System.Drawing.Color.Black;
                    var newItemBackColor = item.Value["exported"].Equals("yes") ?
                        System.Drawing.Color.Azure : System.Drawing.Color.White;
                    var newItem = new ListViewItem(
                        items.ToArray(), null, newItemForeColor, newItemBackColor, null);

                    sheetView.Items.Add(newItem);
                }

                sheetView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                sheetView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }

        private void selectSheetToolStripMenuItem_Click(object sender, ToolStripItemClickedEventArgs eventArgs)
        {
            currentSheetLabel.Text = eventArgs.ClickedItem.Text;
            PrintSheet();
        }

        private void exportButton_Click(object sender, System.EventArgs e)
        {
            foreach (ListViewItem item in sheetView.CheckedItems.OfType<ListViewItem>().ToArray())
            {
                item.BackColor = System.Drawing.Color.Azure;
            }
        }

        /// <summary>
        /// Helper method to determin if invoke required, if so will rerun method on correct thread.
        /// if not do nothing.
        /// </summary>
        /// <param name="c">Control that might require invoking</param>
        /// <param name="a">action to preform on control thread if so.</param>
        /// <returns>true if invoke required</returns>
        public bool ControlInvokeRequired(Control c, System.Action a)
        {
            if (c.InvokeRequired)
            {
                c.Invoke(new MethodInvoker(delegate { a(); }));
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}
