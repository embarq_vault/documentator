﻿using System;

namespace SheetAPIAdapter
{
    public class Adapter
    {
        public static void Main(string[] args)
        {
            Drive driveClient = new Drive();
            Spreadsheet spreadsheet = null;
            String sheetId = driveClient.GetFileByName("test-sheet");
            try
            {
                spreadsheet = new Spreadsheet(sheetId);
                spreadsheet.Print();
                
            }
            catch (System.IO.FileNotFoundException err)
            {
                Console.WriteLine(err.Message);
            }

            Console.ReadLine();
        }
    }
}