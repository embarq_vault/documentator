﻿using System.IO;
using System.Threading;
using Google.Apis.Drive.v3;
using Google.Apis.Sheets.v4;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;

namespace SheetAPIAdapter
{
    class Credentials
    {
        public UserCredential Data { get; }

        public Credentials()
        {
            string[] Scopes = {
                SheetsService.Scope.SpreadsheetsReadonly,
                DriveService.Scope.Drive
            };
            
            using (var stream =
                new FileStream(
                    Config.SecretConfigPath, 
                    FileMode.Open, 
                    FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(
                    credPath, 
                    Config.CredentialsPath);

                Data = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }
        }
    }
}
