﻿using System;
using System.Collections.Generic;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;

namespace SheetAPIAdapter
{
    public class Spreadsheet
    {
        string Id;
        SheetsService service;
        /// <summary>
        /// Key - sheet name, Value - sheet content
        /// </summary>
        public Dictionary<string, Sheet> Sheets { get; }
        /// <summary>
        /// List of sheet's name
        /// </summary>
        public List<string> Titles { get; }
        
        public Spreadsheet(String sheetName)
        {
            Drive driveClient = new Drive();
            Id = driveClient.GetFileByName(sheetName);
            Titles = new List<string>();
            Sheets = new Dictionary<string, Sheet>();
            service = new SheetsService(
                new BaseClientService.Initializer()
                {
                    HttpClientInitializer = new Credentials().Data,
                    ApplicationName = Config.AppName
                });
            
            var request = service.Spreadsheets.Get(Id);
            request.Fields =
                @"sheets.data.rowData.values.effectiveValue.stringValue,sheets.properties.title";
            var response = request.Execute();

            foreach (var sheet in response.Sheets)
            {
                try
                {
                    Titles.Add(sheet.Properties.Title);
                    Sheets.Add(
                        sheet.Properties.Title,
                        new Sheet(sheet.Data[0].RowData));
                }
                catch (NullReferenceException)
                {
                    Sheets.Add(sheet.Properties.Title, new Sheet());
                }
            }
        }

        public Sheet Get(int index)
        {
            ArgumentOutOfRangeException exception = new ArgumentOutOfRangeException();

            if (index >= Sheets.Count || index < 0)
            {
                throw exception;
            }

            for (short i = 0; i < Sheets.Count; i++)
            {
                if (i == index)
                {
                    return Sheets[Titles[i]];
                }
            }

            throw exception;
        }

        public void Update()
        {
        }

        public void Print()
        {
            foreach (var sheet in Sheets)
            {
                Console.WriteLine("Sheet \"{0}\", Items: {1}", 
                    sheet.Key, sheet.Value.Data.Count);
                foreach (var row in sheet.Value.Data)
                {   
                    Console.WriteLine("\tItem: {0}", row.Key);
                    foreach (var col in row.Value)
                    {
                        Console.WriteLine("\t\t{0}: {1}", 
                            col.Key, col.Value);
                    }
                }
                Console.WriteLine("");
            }
        }
    }
}
