﻿using System.Collections.Generic;
using Google.Apis.Sheets.v4.Data;
using Newtonsoft.Json;

namespace SheetAPIAdapter
{
    public class Sheet
    {
        /// <summary>
        /// Key for this dictionary must be special semantic value that best identifies the subset(Value)
        /// </summary>
        public Dictionary<string, Dictionary<string, string>> Data { get; }
        public List<string> Captions { get; }

        public Sheet()
        {
            Data = new Dictionary<string, Dictionary<string, string>>();
            Captions = new List<string>();
        }

        // TODO: Пересмотреть поле `Sheet.Data` т.к. СД хранилища является 
        // словарь уникальных ключей и соответствующих ему значений,
        // а ключи могут повторяться
        public Sheet(IList<RowData> data)
        {
            Data = new Dictionary<string, Dictionary<string, string>>();
            Captions = new List<string>();
                
            foreach (var item in data[0].Values)
            {
                Captions.Add(item.EffectiveValue.StringValue);
            }

            data.RemoveAt(0);

            foreach (RowData row in data)
            {
                var subset = new Dictionary<string, string>();

                foreach (CellData cell in row.Values)
                {
                    int captionindex = row.Values.IndexOf(cell);
                    string cellValue = cell.EffectiveValue.StringValue;
                    string cellLabel = Captions[captionindex];  // Falls down when coll hasn't caption

                    subset.Add(cellLabel, cellValue);
                }
                
                // Means that the Key of subset/item must consist of
                // two most important fields of subset 
                Data.Add( 
                    subset[Captions[0]],
                    subset);
            }
        }

        /// <summary>
        /// Get sheet row by specific key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Dictionary<string, string> Get(string key)
        {
            return Data[key];
        }

        public string GetRowAsString(string key)
        {
            return JsonConvert.SerializeObject(Data[key]);
        }
    }
}
