﻿namespace SheetAPIAdapter
{
    class Config
    {
        public const string AppName = @"documentator";
        public const string SecretConfigPath = @"../../../client_secret.json";
        public const string CredentialsPath = @".credentials/sheets.googleapis.com-documentator.json";
        public const string DefaultSpreadSheet = @"1_w_ZVEzBHJVXALkKSjHq2YO1FcqZrId3LIr0_MVa49M";
    }
}
