﻿using System;
using Google.Apis.Services;
using Google.Apis.Drive.v3;

namespace SheetAPIAdapter
{
    public class Drive
    {
        DriveService service;
        
        public Drive()
        {
            service = new DriveService(
                new BaseClientService.Initializer()
                {
                    HttpClientInitializer = new Credentials().Data,
                    ApplicationName = Config.AppName
                });
        }

        public String GetFileByName(String name)
        {
            FilesResource.ListRequest request =
                service.Files.List();

            var response = request.ExecuteAsync().Result;

            foreach (var file in response.Files)
            {
                if (file.Name == name)
                {
                    return file.Id;
                }
            }

            throw new System.IO.FileNotFoundException(
                String.Format(
                    "File which name is \"{0}\" not present on your Drive",
                    name));
        }
    }
}
